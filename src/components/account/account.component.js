import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAccount } from '../../actions';

class Account extends Component {

    componentWillMount() {
        this.props.fetchAccount();
    }
    render() {
        return (
            <div>
                <h4 className="mb-3">Account Information</h4>
                <dl>
                    <dt>Name: </dt>
                    <dd>{this.props.account.name}</dd>
                    <dt>Current Address: </dt>
                    <dd>{this.props.account.wallets.BTC.address}</dd>
                    <dt>Balance: </dt>
                    <dd>{this.props.account.wallets.BTC.balance} BTC</dd>
                </dl>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    account: state.account
});

export default connect(mapStateToProps, { fetchAccount })(Account);
