import React, { Component } from 'react';
import PropsTypes from 'prop-types';
import { connect } from 'react-redux';
import { tradeSend } from '../../actions';

class SendForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            address: '',
            amount: '',
            fee: 0,
            total: 0
        }
    }

    onChangeAdrress = (e) => {
        const address = e.target.value;
        this.setState({
            address
        });
    }

    onChangeAmount = (e) => {
        const amount = parseFloat(e.target.value ? e.target.value : '0');
        const fee = 0.000001;
        const total = (isNaN(amount) ? 0 : amount) + fee;
        this.setState({
            amount: e.target.value,
            fee: fee,
            total: total
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        const transaction = {
            amount: parseFloat(this.state.amount),
            fee: this.state.fee,
            address: this.state.address
        };

        this.props.tradeSend(transaction);
    }

    getFee(amount) {

    }

    render() {
        const validAddrress = this.props.trade.status === 400 ? 'is-invalid' : '';
        return (
            <div>
            {
                this.props.trade.transaction === 'OK' ?
                <div className="alert alert-success" role="alert">
                    Transaction Completed!!
                </div>
                :
                <form onSubmit={this.onSubmit}>
                    <h4 className="mb-3">Send BTC</h4>
                    {
                        this.props.trade.transaction === 'FAIL' ?
                        <div className="alert alert-danger" role="alert">
                            {this.props.trade.reason}
                        </div>
                        :null
                    }
                    <div className="row">
                        <div className="col-md-6 mb-3">
                            <label htmlFor="address">Destination Address</label>
                            <input className={'form-control ' + validAddrress} name="address" type="text" value={this.state.address} onChange={this.onChangeAdrress} />
                        </div>
                        <div className="col-md-6 mb-3">
                            <label htmlFor="amount">Amount to send</label>
                            <input className="form-control" name="amount" type="text" value={this.state.amount} onChange={this.onChangeAmount} />
                        </div>
                    </div>            
                    <dl>
                        <dt >Fee</dt>
                        <dd>{this.state.fee}</dd>
                        <dt>Total</dt>
                        <dd>{this.state.total}</dd>
                    </dl>
                    <button type="submit" className="btn btn-primary btn-lg btn-block">Submit</button>
                </form>
            }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    trade: state.trade
})

SendForm.propsTypes = {
    tradeSend: PropsTypes.func.isRequired,
    trade: PropsTypes.object.isRequired,
}

export default connect(mapStateToProps, { tradeSend })(SendForm);
