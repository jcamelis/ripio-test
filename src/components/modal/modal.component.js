import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap4-modal';

class LoadingModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            loading: nextProps.trade.loading
        })
    }

    render() {
        return (
            <Modal visible={this.state.loading} onClickBackdrop={this.modalBackdropClicked}>
                <div className="modal-body">
                    <p>Loading...</p>
                </div>
            </Modal>
        )
    }
}
const mapStateToProps = state => ({
    trade: state.trade
});

export default connect(mapStateToProps, { })(LoadingModal);
