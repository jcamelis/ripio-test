import React, { Component } from 'react';
import './App.css';

import Account from './components/account/account.component';
import SendForm from './components/trade/send-form.component';
import LoadingModal from './components/modal/modal.component';

class App extends Component {
  render() {
    return (
      <div className="App container">
        <LoadingModal />
        <header className="App-header">
          Ripio Test
        </header>
        <div className="row">
          <div className="col-md-4 order-md-2 mb-4">
            <Account />
          </div>
          <div className="col-md-8 order-md-1">          
            <SendForm />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
