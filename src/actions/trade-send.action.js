import axios from 'axios';
import { TRADE_SEND, TRADE_COMPLETE, TRADE_SEND_ERROR, TRADE_SEND_TIMEOUT, FETCH_ACCOUNT } from './types';

export const tradeSend = (transaction) => (dispatch) => {

    dispatch({
        type: TRADE_SEND,
        payload: {
            loading: true
        }
    })

    axios({
            url: 'http://localhost:3001/trade/send/btc',
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            data: transaction,
            timeout: 2000
        })
        .then((response) => {
            const transactionResponse = {
                reason: response.data.reason,
                transaction: response.data.transaction
            };
            dispatch({
                type: FETCH_ACCOUNT,
                payload: response.data.account
            });
            dispatch({
                type: TRADE_COMPLETE,
                payload: transactionResponse
            });
        })
        .catch(error => {
            console.log(error);
            if (error.code === "ECONNABORTED" || error.response.status === 504) {
                dispatch({
                    type: TRADE_SEND_TIMEOUT,
                    payload: error
                });
            } else if (error.response.status === 400) {
                dispatch({
                    type: TRADE_SEND_ERROR,
                    payload: {
                        ...error.response.data,
                        status: 400
                    }
                });
            }
        });
}

export default tradeSend;
