import axios from 'axios';
import { FETCH_ACCOUNT } from '../actions';

export const fetchAccount = () => dispatch => {
    axios.get('http://localhost:3001/user/')
        .then(res => res.data)
        .then((account) => dispatch({
                type: FETCH_ACCOUNT,
                payload: account
            })
        );
}

export default fetchAccount;
