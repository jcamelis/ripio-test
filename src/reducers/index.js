import { combineReducers } from 'redux';
import { AccountReducer } from './account.reducer';
import { TradeReducer } from './trade.reducer';

export default combineReducers({
    account: AccountReducer,
    trade: TradeReducer
})