import { TRADE_SEND, TRADE_COMPLETE, TRADE_SEND_ERROR, TRADE_SEND_TIMEOUT } from '../actions';

const initialState = {
    loading: false
};

export const TradeReducer = (state = initialState, action) => {
    switch(action.type) {
        case TRADE_SEND: 
            return {
                ...state,
                loading: true
            }
        case TRADE_COMPLETE: 
            return {
                ...state,
                ...action.payload,
                loading: false
            }
        case TRADE_SEND_ERROR: {
            return {
                ...state,
                ...action.payload,
                loading: false
            } 
        }
        case TRADE_SEND_TIMEOUT: {
            return {
                ...state,
                transaction: 'FAIL',
                reason: 'Tiempo de espera agotado.',
                loading: false
            }
        }

        default: return state;
    }
}

export default TradeReducer;