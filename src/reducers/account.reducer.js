import { FETCH_ACCOUNT } from '../actions';

const initialState = {};

export const AccountReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ACCOUNT: 
            console.log(action.payload);
            return {
                ...state,
                ...action.payload
            }

        default: return state;
    }
}

export default AccountReducer;